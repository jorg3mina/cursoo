const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');


let mailConfig;
// Env HEROKU
if (process.env.NODE_ENV === "production") {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET,
        },
    };
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === "staging") {
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET,
            },
        };
        mailConfig = sgTransport(options);
    } else {
        mailConfig = {
            host: "smtp.ethereal.email",
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            },
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);



// const mailConfig = {
// 	host: 'smtp.ethereal.email',
// 	port: 587,
// 	auth:{
// 		user:'dszfks4wymjsnvio@ethereal.email', 
// 		pass: 'p9xgyUfPqk9yHytusn'
// 	}
// };

// // const mailConfig = nodemailer.createTransport({
// //   service: 'gmail',
// //   auth: {
// //       user:  'bicicletajonas@gmail.com', 
// //       pass:  'red_biciCali123'
// //   }
// // });

// module.exports = nodemailer.createTransport(mailConfig)
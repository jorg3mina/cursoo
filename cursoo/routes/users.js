var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuarios');

router.get('/', usuarioController.list);
router.get('/create', usuarioController.create_get);
router.post('/create', usuarioController.create);
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update', usuarioController.update);
router.post('/:id/delete', usuarioController.delete);


/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

module.exports = router;
